##################################################################
# Generic startup template for RFLPS Fast Interlock Module IOC
#
# Example IOC - last modified in 2020-02-08
#
##################################################################

##################################################################
# Required modules and versions
##################################################################
require asyn,4.36.0
require nds3epics,1.0.1
require scaling,1.6.2
require autosave,5.10.0
require iocStats,3.1.16
require recsync,1.3.0-9705e52

### Main module of the IOC
require fimioc,1.0.0-rc1

##################################################################
# Environmental variables settings
##################################################################

### EPICS configurations
epicsEnvSet(EPICS_CA_MAX_ARRAY_BYTES, 10000000)
epicsEnvSet(SMNM, 8000)
epicsEnvSet(NELM, 8000)

### External file with individual macros for all input channels
iocshLoad("devicenames.iocsh")

### SEC-SUB prefix
epicsEnvSet(PREFIX,"RFQ-010RFC:RFS01")

### Calibration Files Folder
epicsEnvSet("CALIB_FOLDER", "$(PWD)/calibration")

##################################################################
# IFC1410 EPICS driver for RFLPS FIM firmware
##################################################################
ndsCreateDevice(ifcfastint, $(PREFIX), card=0, fmc=1, files=$(CALIB_FOLDER))
dbLoadRecords(FIMIOC_Main.template, "PREFIX=$(PREFIX), DEVICE=$(IOCDEVICE), NDSROOT=$(PREFIX), NDS0=FSM, SMNM=$(SMNM)")

##################################################################
# Analog Inputs Records
##################################################################
iocshLoad("dbLoad_aiChannelsRecs.iocsh")

##################################################################
# Digital Inputs Records
##################################################################
iocshLoad("dbLoad_diChannelsRecs.iocsh")

##################################################################
# Process triggers for autosave restored PVs
##################################################################
iocshLoad("$(fimioc_DIR)/restore_pvs.iocsh")
afterInit("dbpf $(PREFIX):RestoreAll 1")

##################################################################
# Calc records for real-time preconditions evaluation
##################################################################
iocshLoad("$(fimioc_DIR)/preconditions.iocsh")

##################################################################
# autosave Module setup
##################################################################
#epicsEnvSet("AS_REMOTE", "/opt/nonvolatile/mtca01-ifc01-rf-ts2/autosave/$(PREFIX)")
epicsEnvSet("AS_REMOTE", "$(E3_CMD_TOP)")
epicsEnvSet("AS_FOLDER", "savfiles")
iocshLoad("$(autosave_DIR)/autosave.iocsh", "AS_TOP=$(AS_REMOTE),IOCNAME=$(AS_FOLDER)")

##################################################################
# iocStats Module setup
##################################################################
epicsEnvSet("IOCSTATSPREFIX", "RFLAB-01")
iocshLoad("$(iocStats_DIR)/iocStats.iocsh", "IOCNAME=$(IOCSTATSPREFIX)")

##################################################################
# recSync Module setup
##################################################################
epicsEnvSet("RECSYNCPREFIX", "RFLAB-01")
iocshLoad("$(recsync_DIR)/recsync.iocsh", "IOCNAME=$(RECSYNCPREFIX)")


##################################################################
# Sequencer setup
##################################################################
afterInit('seq fimioc_automation_seq "PREFIX=$(PREFIX), DEVICE=$(IOCDEVICE)"') 


##################################################################
# Launch IOC engine
##################################################################
iocInit()

