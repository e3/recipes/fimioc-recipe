# fimioc conda recipe

Home: "https://gitlab.esss.lu.se/rflps/fim/fimioc"

Package license: EPICS Open License

Recipe license: BSD 3-Clause

Summary: RFLPS Fast Interlock Module
